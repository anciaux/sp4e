#include "riemann_integral.hh"
/* -------------------------------------------------------------------------- */

RiemannIntegral::RiemannIntegral(std::function<double(double)> func, double inf, double sup) {
  this->func = func;
  this->inf = inf;
  this->sup = sup;
}

/* -------------------------------------------------------------------------- */  

double RiemannIntegral::compute(UInt N){

  this->current_value = 0.;
  this->current_term = 0.;
  
  for (UInt k = 0; k < N; ++k) {
    ++current_term;
    double a = current_term * (sup-inf);
    double b = N;
    double new_current_term = inf + (a/b);
    current_value += (sup-inf)/N * func(new_current_term);
  
  }

  return current_value;
  
}
