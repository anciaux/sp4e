#ifndef __RIEMANN_INTEGRAL_HH__
#define __RIEMANN_INTEGRAL_HH__
/* -------------------------------------------------------------------------- */
#include "series.hh"
/* -------------------------------------------------------------------------- */

class RiemannIntegral : public Series {
public:
  RiemannIntegral(std::function<double(double)> func, double inf, double sup);

  double compute(UInt N) override;

  std::function<double(double)> func;
  double inf;
  double sup;
};

#endif /* __RIEMANN_INTEGRAL_HH__ */
