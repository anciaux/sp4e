# Scientific Programming for Engineers

![](http://phdcomics.com/comics/archive/phd053104s.gif)

## Summary

The students will acquire a solid knowledge on the processes necessary to design, write and use scientific software. Software design techniques will be used to program a multi-usage particles code, aiming at providing the link between algorithmic/complexity, optimization and program designs.
Content

- Object Oriented Paradigm
- C/C++ and Python programming (class, operator, template, design patterns, STL)
- Programming techniques, code factorization
- Pointers, memory management, data structures
- Linear system solving (Eigen library)
- C++/Python coupling (pybind)
- Post-treatment: Paraview, numpy/scipy, matplotlib


## Classical problems:

- series calculations
- solar system and many-body calculation
- sparse linear algebra.
