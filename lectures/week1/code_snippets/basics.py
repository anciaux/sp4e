#### making comments

#my super comment

### command print

print(10)

### type

a = 10
b = 10.
type(a)
type(b)

### Converting string to integer or float

a = "1.2"
print(a)
a_float = float(a)
print(a_float)
# a_int = int(a) # raises an error
a_int = int(float(a))
print(a_int)

### list

a = list()
a.append(2)
print(a)
a = [2,4,5]
a.append([2,4,5])
print(a)
a += [2,4,5]
b = [1,2]*4
print(a[1])
print(a[2])

### maps/dictionaries

planet = dict()
planet["name"] = "mars"  
planet["radius"] = 1.2
planet["mass"] = 0.4
print(planet)


### if conditions

a = 1
if not a == 0:
    print("a != 0")
else:
    print("a == 0")

### for loops (beware the indentation)

for i in range(0,10):
    print(i)


mylist = [1, 2, 3, 4]
for i in mylist:
    print(i)

### defining function

def foo(arg1, arg2, arg3):
    # ... 
    some_code
    # ...


### opening files for writing


f = open("my_super_filename.csv", 'w')
f.write("#X Y Z VX VY VZ FX FY FZ mass radius name\n")
f.close()


### opening a file and read line by line


f = open("my_super_filename.csv", 'r')
for line in f:
    entries = line.split()
    print(entries)