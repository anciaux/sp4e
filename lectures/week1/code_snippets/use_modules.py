### using other modules

# request to use another module
import sys
import my_module

res = my_module.foo(10)
print(res)

### how to get the arguments passed to the program ?

# the arguments are stored in the list sys.argv
# for instance you can print it with

print(sys.argv)