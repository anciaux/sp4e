import argparse

# # *argparse* module

# A module to parse arguments passed to your program
#
# [Argparse: documentation](https://docs.python.org/3/library/argparse.html)
#
# [Argparse: tutorial](https://docs.python.org/3.6/howto/argparse.html)

# ## Basic usage
#
# - simple creation of parser
#
parser = argparse.ArgumentParser()
#

# - Adding arguments
#
#
parser.add_argument('echo', type=str,
                    help='will print this parameter to screen')
parser.add_argument('--verbose', action='store_true',
                    help='will increase verbosity')
parser.add_argument('--factor', type=float, default=1,
                    help='specify a factor')

# - Effective parsing
#
args = parser.parse_args()
print(args.echo, args.factor)
