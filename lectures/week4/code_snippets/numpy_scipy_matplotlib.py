from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import scipy.optimize
import numpy as np


# # <center>Python complements </center>

# ## Default parameters of functions
#
def foo(a, b=1):
    return a+b

# ## *args* and *kwargs*
#
# - **args**: list containing un-named arguments
# - **kwargs**: dictionary containing the named arguments
#


def foo(a, *args, **kwargs):
    print('args:', args)
    print('kwrags:', kwargs)
    return a+1


foo(1, 2, 3, toto=3, tata=4)

# ## lambda functions: for_each
#


def foo(a):
    print(a*10)


l = range(1, 4)


def for_each(_list, func):
    for i in _list:
        func(i)


for_each(l, foo)
for_each(l, lambda x: print(x*10))

# ## lambda functions: transform
#
applied = [foo(i) for i in l]
applied = [(lambda x: x*10)(i) for i in l]

# ## Creating multi-dimentional array zero-filled
#
m = np.zeros((2, 3))

# ## Creating  multi-dimentional array from list/tuple
#
l = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
m = np.array(l)

t = ((1, 2, 3), (4, 5, 6), (7, 8, 9))
m = np.array(t)

# ## Creating special matrix
#
# # Identity matrix
m = np.eye(3)
# # Matrix filled with ones
np.ones((2, 3))
# # diagonal matrix
m = np.diag((2, 3, 4))
# # random matrix
np.random.rand(2, 3)

# ## [Numpy Slicing](https://docs.scipy.org/doc/numpy-1.13.0/reference/arrays.indexing.html)
m = np.random.rand(4)
# - Slicing syntax: m[start:end:stride]
#

#
#
m[2]  # access index 2
m[-1]  # access last index
m[:2]  # sub vector m[0],m[1]
m[1:]  # access m[1], m[2], m[3]
m[::2]  # access even indexes
m[1::2]  # access odd indexes
m[::-1]  # access in decreasing order

# ##  Component-based algebra
#
m = np.ones((3, 3))

# #component by component operation
n = m*2
print(m, n)
print(m+n)
print(m*n)
print(m+1)
print((m-n)**2)

# ## [np.array.shape](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html)
#
# - Size/Dimension of a vector/matrix/tensor is its **shape**
# - It is a tuple
#
m = np.random.rand(2, 3)
print(m)
print(m.shape, type(m.shape))

# In[2]:


m = np.random.rand(2, 3)
print(m)
print(m.shape, type(m.shape))


# ## [flatten](https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.ndarray.flatten.html)
#
print(m)
# # makes a flat copy of m
flat = m.flatten()
print(flat.shape, flat)

# In[3]:


print(m)
# makes a flat copy of m
flat = m.flatten()
print(flat.shape, flat)


# ## [reshape](https://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html#numpy.reshape)
#
# # makes a reshaped view of m
reshaped = m.reshape((3, 2))
print(reshaped)

# In[4]:


reshaped = m.reshape((3, 2))
print(reshaped)


# ## [Scipy linear algebra routines](http://docs.scipy.org/doc/numpy/reference/routines.linalg.html)
#
m = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
n = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
v = np.array([1, 1, 1])
#
# # transposition
m2 = m.T
#
# # matrix-matrix operation
a = np.dot(m, n)
a = m@n
#
# # matrix-vector operation
v2 = np.dot(m, v)
v2 = m@v
#
# #matrix inversion
m = np.eye(3)
print(m)
np.linalg.inv(m)

# ## [Numpy summations](https://docs.scipy.org/doc/numpy/reference/generated/numpy.sum.html)
#
m = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
#
# - $\sum_{i,j} m_{ij}$
m.sum()

#
# - $\sum_{i} m_{ij}$ and $\sum_{j} m_{ij}$
#
m.sum(axis=0)
m.sum(axis=1)

# - norm: $\sqrt{\sum_{ij} m_{ij}^2}$
#
np.sqrt((m**2).sum())
#
#

# ## [Einsum](https://docs.scipy.org/doc/numpy/reference/generated/numpy.einsum.html)
#
# - Tensor product with einstein notation
# - mat-vec product: $u_i = m_{ik} v_k$
#
u = np.einsum('ik,k->i', m, v)
#
# - dot product: $norm = v_k v_k$
#
norm = np.einsum('k,k->', v, v)
#
# - Transposition
#
np.einsum('ij->ji', m)
#
#

# # [Scipy optimization](http://docs.scipy.org/doc/scipy/reference/optimize.html)
#
#
# with a lambda
ret = scipy.optimize.minimize(lambda x: ((x-1)**2).sum(),
                              0.,
                              tol=1e-9)

# # without a lambda


def foo(x, center):
    return (x-center)**2


ret = scipy.optimize.minimize(foo, 0., args=[1, ], tol=1e-9)


def foo(x, center):
    return (x-center)**2


ret = scipy.optimize.minimize(foo, 0., args=[1, ], tol=1e-9)
# - Return of the function gives information about the convergence:
#

# In[5]:


scipy.optimize.minimize(lambda x: ((x-1)**2).sum(),
                        0.,
                        tol=1e-9)


# # [Matplotlib](http://matplotlib.org/contents.html)
#
# - 2D/3D plotting library
# - publication quality figures
# - Combined with Numpy/Scipy: gets post-treatment close to figure scripts
#

# ## [Figure](https://matplotlib.org/api/_as_gen/matplotlib.figure.Figure.html)&[Axe](https://matplotlib.org/api/axes_api.html#matplotlib.axes.Axes) creation
#
# Figure object
fig = plt.figure()
# Axe object
# axe = fig.add_subplot(nrows, ncols, n_plot)

# - Assumes a grid of plots $nrows \times ncols$
# - Returns plot asociated to *n_plot* (row major count)
#
# - For a single plot:

axe = fig.add_subplot(1, 1, 1)
axe = fig.add_subplot(111)


# ## The [plot](https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.plot.html#matplotlib.axes.Axes.plot) function
#
# - takes 2 numpy arrays, one for x one for y
#
x = np.arange(0, 10)
y = x**2
axe.plot(x, y)

# Display/Save figures
plt.show()
plt.savefig("figure.pdf")

# In[6]:


x = np.arange(0, 10)
y = x**2
fig = plt.figure()
axe = fig.add_subplot(111)
ret = axe.plot(x, y)


x = np.arange(0, 10)
y = x**2
fig = plt.figure()
axe = fig.add_subplot(111)
ret = axe.plot(x, y)

# Axes labels
axe.set_xlabel("X axis")
axe.set_ylabel("$X^2$")

# Axes ranges
ymin = 0
ymax = 1
xmin = 0
xmax = 1
axe.set_ylim((ymin, ymax))
axe.set_xlim((xmin, xmax))

# In[7]:


fig = plt.figure()
axe = fig.add_subplot(111)
axe.set_xlabel("X axis")
axe.set_ylabel("$X^2$")
ret = axe.plot(x, y)


# ## Curves legend
#
x = np.arange(0, 10)
y1 = x**2
y2 = x**3
y3 = x**4
axe.plot(x, y1, label="$x^2$")
axe.plot(x, y2, label="$x^3$")
axe.plot(x, y3, label="$x^4$")
axe.legend()
#

# In[8]:


fig = plt.figure()
axe = fig.add_subplot(1, 1, 1)
x = np.arange(0, 10)
y1 = x**2
y2 = x**3
y3 = x**4
axe.plot(x, y1, label="$x^2$")
axe.plot(x, y2, label="$x^3$")
axe.plot(x, y3, label="$x^4$")
ret = axe.legend()


# ## Line style
#
# line only
axe.plot(x, y1, '-',
         label="$x^2$")

# points only
axe.plot(x, y2, 'o',
         label="$x^2$")

# lines points
axe.plot(x, y3, 'o-',
         label="$x^2$")

# In[9]:


fig = plt.figure()
axe = fig.add_subplot(1, 1, 1)
x = np.arange(0, 10)
y1 = x**2
y2 = x**3
y3 = x**4
axe.plot(x, y1, '-', label="$x^2$")
axe.plot(x, y2, 'o', label="$x^3$")
axe.plot(x, y3, 'o-', label="$x^4$")
ret = axe.legend()


# ## [Axes 3D](https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html)
#

fig = plt.figure()
axe = Axes3D(fig)

theta = np.linspace(
    -4 * np.pi, 4 * np.pi,
    100)
z = np.linspace(-2, 2, 100)
r = z**2 + 1
x = r * np.sin(theta)
y = r * np.cos(theta)
axe.plot(x, y, z)

# In[10]:


fig = plt.figure()
axe = Axes3D(fig)
theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
z = np.linspace(-2, 2, 100)
r = z**2 + 1
x = r * np.sin(theta)
y = r * np.cos(theta)
ret = axe.plot(x, y, z)


# ## Matplotlib documentation links
#
# [Matplotlib: figure](http://matplotlib.org/api/figure_api.html?highlight\%3Dfigure#module-matplotlib.figure)
#
# [Matplotlib: Axes](http://matplotlib.org/api/axes_api.html#matplotlib.axes.Axes)
#
# [Matplotlib: Axes.plot function](http://matplotlib.org/api/axes_api.html#matplotlib.axes.Axes.plot)
#
# [Matplotlib: Gallery](http://matplotlib.org/gallery.html)
#
# [Matplotlib: 3D tutorial](https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html)
