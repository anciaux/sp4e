class A {
public:
  double value;
};

class B : public A {};

double foo(A &a) { return a.value; }

int main() {
  B b;
  // C++ is strongly typed
  // can I do that ?
  foo(b);
}