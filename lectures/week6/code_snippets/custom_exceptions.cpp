#include <iostream>
#include <sstream>
#include <stdexcept>

class MyError : public std::exception {
public:
  MyError(int value) {
    // set the internal error code
    this->parameter = value;
  }

  const char *what() const noexcept override {
    // construct the string message
    std::stringstream sstr;
    sstr << "Error Code: " << parameter;
    return sstr.str().c_str();
  }
  int parameter;
};

int main() {
  // usage in context
  try {
    throw MyError(-1);
  } catch (MyError &e) {
    std::cerr << e.what() << std::endl;
  }
}