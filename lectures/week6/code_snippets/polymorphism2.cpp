class A {
public:
  virtual double getValue() { return 0.; };
};

class B : public A {

public:
  double getValue() override { return value; };

private:
  double value;
};

double foo(A &a) { return a.getValue(); }

int main() {

  B b;
  foo(b);
}