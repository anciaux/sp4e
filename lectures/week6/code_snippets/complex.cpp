struct Complex {
  Complex operator+(Complex &other);
  double real;
  double imag;
};

int main() {
  Complex a, b;
  auto c = a + b;
}

Complex Complex::operator+(Complex &other) {
  Complex res;
  res.real = this->real + other.real;
  res.imag = this->imag + other.imag;
  return res;
}